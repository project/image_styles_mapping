# Image Styles Mapping

This module allows you to have a report listing the image styles per image
fields and per view modes on all entities.

The report can be accessed at `admin/reports/image_styles_mapping_report`.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no settings or configuration.
